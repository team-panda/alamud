from .event import Event2, Event3


class UseEvent(Event2):
    NAME = "use"

    def perform(self):
        if not self.object.has_prop("usable"):
            self.fail()
            return self.inform("use.failed")
        self.inform("use")



class UseOnEvent(Event3):
    NAME = "use-on"

    def perform(self):
        if not self.object.has_prop("usable-on"):
            self.fail()
            return self.inform("use-on.failed")
        self.inform("use-on")