from .action import Action2, Action3
from mud.events import UseEvent, UseOnEvent

class UseAction(Action2):
    EVENT = UseEvent
    ACTION = "use"
    RESOLVE_OBJECT = "resolve_for_use"

class UseOnAction(Action3):
    EVENT = UseOnEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"
    ACTION = "use-on"